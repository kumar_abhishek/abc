import React from 'react';
import Loading from '../components/loading';
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Link,
	Redirect
} from 'react-router-dom';
import Home from '../routes/home';
import Drivers from '../routes/drivers';
import Teams from '../routes/teams';
import Nav from '../components/nav';

export default class App extends React.Component {
	constructor(props) {
		super(props);
		this.appDom = document.querySelector('#app');
		this.state = {
			loading: true,
			city: ''
		};
		this.geocoder = new google.maps.Geocoder();
		this.codeLatLng = this.codeLatLng.bind(this);
		this.errorFunction = this.errorFunction .bind(this);
		this.successFunction = this.successFunction.bind(this);
	}

	componentDidMount(){
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(this.successFunction, this.errorFunction);
		}
		this.setState({loading: false, user: null});
		
		setTimeout(()=> {
			const dom = document.querySelector('#app');
			dom.classList.remove('visibilityh');
		}, 200);
	}

	//Get the latitude and the longitude;
	successFunction(position) {
		var lat = position.coords.latitude;
		var lng = position.coords.longitude;
		this.codeLatLng(lat, lng)
  	}
  
	  
	errorFunction(){
		alert("Geocoder failed");
  	}

	codeLatLng(lat, lng) {

		var latlng = new google.maps.LatLng(lat, lng);
		this.geocoder.geocode({'latLng': latlng}, (results, status) => {
		  if (status == google.maps.GeocoderStatus.OK) {
			if (results[1]) {
			//find country name
				for (var i=0; i<results[0].address_components.length; i++) {
					for (var b=0;b<results[0].address_components[i].types.length;b++) {
						if (results[0].address_components[i].types[b] == "locality") {
							this.setState({city: results[0].address_components[i]});
							break;
						}
					}
				}
			} else {
			  console.error("No results found");
			}
		  } else {
			  console.error("Geocoder failed due to: " + status);
		  }
		});
	}

	render() {
		let user = this.state.user;
		if( this.state.loading ){
			return <Loading />;
		} else {
			return (<Router key="router">
				<div className="flex flex1 flexc">
					<Nav key="navigation" />
					<Switch>
						<Route exact path="/" render={(props)=>{ return <Home location={props.location} city={this.state.city} /> }} />
						<Route exact path="/races" render={(props)=>{ return <Home location={props.location} /> }} />
						<Route exact path="/drivers" render={(props)=>{ return <Drivers location={props.location} /> }} />
						<Route exact path="/teams" render={(props)=>{ return <Teams location={props.location} /> }} />
					</Switch>
				</div>
			</Router>);
		}
	}
};
