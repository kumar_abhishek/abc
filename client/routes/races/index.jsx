import React from 'react';
import { Link } from 'react-router-dom';
import LeftBar from '../../components/leftbar';
import NavBar from '../../components/navbar';
import s from './journal.css';

export default ({user, onlogout, onupdatebg}) => {
    onupdatebg('bg3');
    return (<div className={`${s.journal} flex flex1 relative`}>
		<LeftBar user={user}>
            The Divine Tree of Life connects all forms of creation, symbolizing the universal need for growth, and progress in one's spiritual journey.
            <br/>The premium course is designed to guide you through that journey, acting as a primary form of support for the constant development of your mind, body, and soul.
		</LeftBar>
		<div className="content flex flex1 flexc zi0">
			<NavBar user={user} onlogout={onlogout} />
			<div className={` flex flex1 flexc alignc`}>
				<div className={`${s.type} white flex flexc overflow-yauto`}>
                    <h4 className="white relative">Journal Entries</h4>
					<ul>
                    {
                        user.stat.m.map((m, i)=>(
                            <li key={`m${i+1}`} className="flex flexc">
                                <input id={`m${i+1}`} type="checkbox" defaultChecked={i===0} autoFocus name="accordion" />
                                <label className="pointer bg-purple2 fs-1-5r" htmlFor={`m${i+1}`}>Module {i+1}</label>
                                <ul className={`${s.content}`}>
                                {
                                    m.w.map((w, j) => (
                                        <li key={j}>
                                            <Link to={`/module/${i+1}/${j+1}/${user.stat.m[i].w[j].t}`}>Week {j+1}</Link>
                                        </li>
                                    ))
                                }
                                </ul>
                            </li>
                        ))
                    }
                    </ul>
				</div>
			</div>
		</div>
    </div>);
}
