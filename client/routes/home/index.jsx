import React from 'react';

import {Link} from 'react-router-dom';

import s from './home.css';


export default ({user, city}) => {
	if(city){
		city = city.short_name;
	}
	
	return (<div className={`${s.dashboard} flex flex1 relative`}>
		
		<div className="content flex flex1 flexc">
			{`Welcome! there at ${city}`}
		</div>
	</div>);
};
