import React from 'react';
import s from './journal.css';

export default ({j, onJournalEdit }) => {
  return (
    <div className={`${s.journal} flex flexc flex1`}>
      <h5>Journal</h5>
      <p>Use this space to collect your notes and findings.</p>
      <textarea onChange={onJournalEdit} value={j} />
    </div>);
};
