import React from 'react';
import { Route, NavLink as Link } from 'react-router-dom';
import s from './leftbar.css';
const ss = JSON.parse(sessionStorage.getItem('tol'));

export default class LeftBar extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      collapsed: true
    };

    this.onToggleExpand = this.onToggleExpand.bind(this);
    this.onBlur = this.onBlur.bind(this);
  }

  componentDidMount(){
    document.addEventListener('click', this.onBlur);
  }

  componentWillUnmount(){
    document.removeEventListener('click', this.onBlur);
  }

  onToggleExpand(e){
    this.setState({collapsed: !this.state.collapsed});
  }


  onBlur(e){
    if(!e.target.closest('.leftbar')){
      this.setState({collapsed: true});
    }
  }

  render() {
    return (<div className={`leftbar ${s.leftbar} ${this.state.collapsed ? s.collapsed : ''} border-box flex flexc flexs0 overflow-h`}>
    <div className={`${s.archetype}`}>
      <input id="atype" type="checkbox" onChange={this.onToggleExpand} />
      <label htmlFor="atype" className={`${s.atypelabel} ${s[this.props.user.a]}`} />
    </div>
    <div className={`${s.content}`}>
      <div className={`${s.actr}`}><span className={` `}></span></div>
      <span className={`${s.logo}`}></span>
      <h5 className="ff-generica-bold talignc">Divine Tree Of Life</h5>
      <div className="overflow-yauto flex flexc padl1r padr1r padb1r">
        {this.props.children}
        <Link to="/tree" className="btn bg-purple2 fs-1-5r fw-bold white ff-generica mart1r talignc">View My Tree</Link>
        <Link to="/" className="btn bg-purple1 fs-1-5r fw-bold white ff-generica mart1r talignc">Dashboard</Link>
        <Link to="/journal" className="btn bg-green fs-1-5r fw-bold white ff-generica mart1r talignc">Read My Journal</Link>
      </div>
    </div>
  </div>);
  }
};
