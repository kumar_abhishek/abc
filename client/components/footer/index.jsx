import React from 'react';

export default (props) => (
	<div className={`flex flex1 justfe flexc relative fs-0-75r talignc bold`}>
		<div className="alignsc padt0-5r padb0-5r white"><a target="_blank" href="/">Home</a> | <a target="_blank" href="http://individualogist.com/privacy-policy">Privacy Policy</a> | <a target="_blank" href="http://individualogist.com/terms-and-conditions">Terms &amp; Conditions</a> | <a target="_blank" href="http://individualogist.com/contact">Contact</a></div>
	</div>);
