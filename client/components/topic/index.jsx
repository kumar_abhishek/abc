/**
 * Topic Component
 */
import React from 'react';
import Journal from '../journal';
import {getTopic} from '../../utils';
import s from './topic.css';

// Maintain cache for topic fetched
const cache = {};

export default class Topic extends React.Component {
	constructor(props){
    super(props);
    this.state = {
      content: null
    }
    this.fetchContent = this.fetchContent.bind(this);
  }

  fetchContent(m, w, t){
    const dis = this;
    const k = `${m}/${w}${t}`; // k is key for cache having format as 'm/w/t'
    if(t < this.props.stat.m[m-1].w[w-1].t){
      if(cache[k]){
        dis.setState(cache[k]);
      } else {
        getTopic(m, w, t, this.props.fn).then((d)=>{
          cache[k] = d;
          dis.setState(d);
        });
      }
    }
  }

  componentDidMount() {
    const {m, w, t} = this.props;
    console.log('Topic: componentDidMount', m, w, t);
    this.fetchContent(m, w, t);
  }

  
  componentWillReceiveProps(newProps){
    const {m, w, t} = newProps;
    console.log('Topic: componentWillReceiveProps', m, w, t);
    this.fetchContent(m, w, t);
  }

  render() {
    const {m, w, t, j} = this.props;
    if(t === this.props.stat.m[m-1].w[w-1].t){
      return (
      <div className={`${s.topic} flex flex1 flexc ff-nunito fw-thin`}>
        <Journal onJournalEdit={this.props.onJournalEdit} j={j} />
      </div>);  
    } else {
      return (
        <div className={`${s.topic} flex flex1 flexc ff-nunito fw-thin`} 
          dangerouslySetInnerHTML={{__html: this.state.content}} />
      );
    }
  }
};
