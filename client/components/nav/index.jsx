import React from 'react';
import { Route, NavLink as Link } from 'react-router-dom';
import s from './nav.css';


export default (props) => {
  //console.log(s);
  return (
<header className={`${s.nav} flex`}>
  <nav className={`${s.menu} flex flex1 alignc fw-bold`}>
    <ul className="flex flex1 aligns">
      <li className="flex alignc"><Link className={`${s.logo} pad1r`} exact to="/"><div className={``} /></Link></li>
      <li className="flex alignc"><Link to="/drivers">Drivers</Link></li>
      <li className="flex alignc"><Link to="/teams">Team</Link></li>
    </ul>
  </nav>
</header>);
};
