import React from 'react';
import {Link} from 'react-router-dom';
import s from './resume.css';

export default (props) => (
	<div className={`${s.resume} ${(props.r) ? '': s.hidden} talignc`}>
		<h5 className="white">Would You Like To Continue From You Left Off?</h5>
		<div>
			<Link className="btn bg-purple2 fs-1r fw-bold white ff-generica mart1r talignc marr1r" onClick={props.onresumeHandled} to={props.continueUrl}>Continue</Link>
			<button className="btn bg-yellow fs-1r fw-bold ff-generica mart1r talignc" onClick={props.onresumeHandled}>Cancel</button>
		</div>
	</div>);
