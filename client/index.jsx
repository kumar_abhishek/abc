import React from 'react';
import ReactDom from 'react-dom';
import App from './app';
import './assets/css/style.css';
/*

import './assets/img/logo.png';
require('./assets/css/nunito-fontface.css');*/

const appEl = document.querySelector('#app');

let pollyfills = [];

if(!window.Promise){
	pollyfills.push('./promise.js');
}

if(!window.fetch){
	pollyfills.push('./fetch.js');
}

ReactDom.render(<App />, appEl);

if (module.hot) {
	module.hot.accept();
}