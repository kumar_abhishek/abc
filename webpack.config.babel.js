const path = require('path');
const webpack = require('webpack');
const html = require('html-webpack-plugin');
const extract = require('extract-text-webpack-plugin');
const srcPath = path.join(__dirname, 'client');
const buildPath = path.join(__dirname, 'public');

const isDev = (process.env.NODE_ENV === 'development');
const isProd = !isDev;

module.exports = {
    context: srcPath,
    entry: isDev ? {app: [
        'react-hot-loader/patch',
        'webpack-dev-server/client?http://localhost:3000',
        'webpack/hot/only-dev-server',
        './index.jsx'
    ]}: {app: ['./index.jsx']},
    output: {
        filename: 'assets/js/[name].[hash].js',
		chunkFilename: 'chunks/[name].[hash].js',
		path: buildPath,
		publicPath: '/',
		sourceMapFilename: '[file].map',
		hashDigestLength: 5
    },
    module: {
        rules: [
            {
                loader: 'babel-loader',
                test: /\.jsx?$/,
                exclude: /node_modules/
                //exclude: /node_modules(?!\/webpack-dev-server)/,
            },
            {
                test: /\.css$/,
                use: extract.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true,
                                import: false,
                                sourceMap: true,
                                modules: true,
                                importLoaders: 1,
                                localIdentName: '[name]-[local]-[hash:base64:5]',
                                camelCase: true
                            }
                        },
                        {
                            loader: 'postcss-loader'
                        }
                    ]
                }),
            },
			{
				test: /\.(woff|woff2|eot|ttf|otf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				use: {
					loader: 'url-loader',
					query: {
						limit: 1000,
						name: 'assets/fonts/[name]-[hash:base64:5].[ext]'
					}
				}
            },
            {
                test: /\.(png|jpe?g|pdf)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                use: {
                    loader: 'url-loader',
                    query: {
                        limit: 1000,
                        name: '[path]/[name]-[hash:base64:5].[ext]'
                    }
                }
            },
			{
                test: /\favicon.ico$/,
                use: [
					{
					  loader: 'url-loader',
					  options: {
						outputPath: 'public'
					  }  
					}
				]
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
        }),
        new extract({
            filename: 'assets/css/[name].[chunkhash].css',
            ignoreOrder: false,
            allChunks: true
        }),
		new html({
			template: 'index.html',
			favicon: 'assets/img/favicon.ico',
			title: 'ABC Racing Inc.',
			loggedin: '"{{loggedin}}"',
			admin: '"{{admin}}"',
			now: Date.now(),
			minify: {
				collapseWhitespace: true,
				collapseInlineTagWhitespace: true,
				collapseBooleanAttributes: true,
				minifyCSS: true,
				minifyJS: true,
				removeAttributeQuotes: true,
				removeComments: true,
				removeScriptTypeAttributes: true,
				removeStyleLinkTypeAttributes: true,
				caseSensitive: true,
				html5: true
			}
		})
    ],
    devtool: isDev ? 'cheap-module-source-map' : 'source-map',
    devServer: {
        host: 'localhost',
        contentBase: path.join(__dirname, 'public'),
        hot: true,
        historyApiFallback: true,
        compress: false,
        port: 3003
    },
    resolve: {
        modules: ["node_modules", 'src'],
        extensions: ['.js', '.json', '.jsx', '.css']
    },
    stats: 'errors-only'
};
