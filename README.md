# PROPOSAL : `ABC Racing Inc`

## Problem Statement

ABC Racing Company is experiencing a downfall in the people showing interest in their sport. They have decided to increase the fan fare, by revamping their digital presence. They would like to recruit and increase their fan base by reaching out to fans wherever they are. 

They still want to connect to their ageing fan base and provide access to their content (like fixtures, top 5 racers etc.) across all digital platforms.

## Business Requirement

1. Reaching out to fans wherever they are
2. Connect to ageing fan base
3. Responsive/adaptive
4. Progressive
5. Contextual
6. Personalised
7. Bookmark specific sections of the page
8. Different color theme based on geography
9. Main focus are: Mobile/Tablet over 3G/4G
10. Faster page load having lot of HD Images and videos
11. AAA level of accessibility
12. Part of page bookmarkable
13. Degrade gracefully on old browser
14. Analytics


# Architecture

## Frontend

1. Use Service Worker for Offline functionality and cache management.
Refer [here](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker) and [here](https://developers.google.com/web/fundamentals/primers/service-workers/)

2. Use geolocation APIs to provide contextual service.
Refer [here](https://developer.mozilla.org/en-US/docs/Web/API/Geolocation/Using_geolocation) for more details.

3. Set viewport, CSS Flexbox based layout and media queries to achieve responsive web design. Refer [here](https://developers.google.com/web/ilt/pwa/lab-responsive-design) for more details.

4. Use IndexedDB for any offile content storage on browser. Its asynchronous and provide large-scale noSQL indexed storage system. Refer [here](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API) for more details.

5. Use `id` attribute in sections which need to be supported for bookmarak. Sections like news items, team details etc are few candidates for bookmarkable content within complex page with so many contents.

6. Use appropiate pollyfills as and when required to support old browsers. Advanced features of ES6/ES7 can be used with the help of babel transpiler. Refer [here](https://babeljs.io/docs/usage/polyfill/) for more details.

7. Use Google analytics for all insights need for better user experience and understanding user in more details. Refer here(https://www.google.com/analytics/analytics/features/) for various features that are suported.

8. Follow [ARIA](https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA) recommendations for accessibilities while depoloping React.js components.

## End to end Infrastructure

1. Use Cloud infrastructure to build entire system from ground up. For sake of discussion we will be using [Amazon Elastic Container Service](https://aws.amazon.com/ecs/) or ECS which help in getting docker container orchestration.

2. Use microservices approach to break existing monolithic application into smaller managed groups of docker containers. Something like below figure (adaption and source: Amazon ECS):

![alt text](/docs/microservice-containers.png "Microservices")

# Localization

A translation utility for Node.js [node-localize](https://github.com/AGROSICA/node-localize) can be used for all kinds of localization need. This is very important to target audiences from various geographies and culture.

# Performance Strategy

Performanc is one of the biggest requirement of any engineering project but it comes as a bi-product of many activies and during various stage of project. Following are few strategies worth noting for achieving better performance for better user experiences:

## CDN:

    * Better use of CDN for static content with long term(say 1 year) cache validity headers is the first and foremost desire to start acting towards performance.

    * Browser also takes advantages of concurrent requests from multiple domains thereby finishing the asset download faster. CDN being on different domain easily fulfill this browser behavior. Sometimes even vendor libraries like react.js, read-dom.js etc can also be sourced directly from CDN in addition to images and CSS.

    * CDN not only off-load web traffic to main servers but also provide more optimised and faster response based on Edge locations. Refer [here](https://aws.amazon.com/cloudfront/details/) for more details.
    
## Service Worker

    * Smart usage of service worker for front-end caching totally aviod server hit and provide fully offline application capabilities.

    * At high level, it need to be registerd and there after it can intercept all GET request and serve content based on type of caching policy designed.

    * Background sync is another useful usecase of service-worker which allow to schedule any data sending to server beyong the life of a web page. More details [here](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorkerRegistration/sync) and [here](https://developers.google.com/web/updates/2015/12/background-sync).
    
## Redis

Use Redis for session management as its disk-backed in-memory and one of the fastest storage engine. This can take all session and user authentication & authorization traffic.

## Microservices

Microservice architecture allow scalling relational and noSQL databases in isolation with nodejs based web server thereby opening door for increasing performance as and when required.

## CORS enabled servers

All data access to frontend will be catered by CORS enabled API server built using Golang and deployed as API service group in micro-service architecture.

# Putting it All-Together

Technical treatment for ABC Racing company include following aspects:

1. Progressive Webapp (PWA)
2. Nodejs and Golang based Web and API servers.
3. Cloud based orchestration of microservices.
4. Google based Analytics services for insights and business decisions.

## PWA:

### Progressive

By definition, a progressive web app must work on any device and enhance progressively, taking advantage of any features available on the user’s device and browser.
Discoverable. Because a progressive web app is a website, it should be discoverable in search engines. This is a major advantage over native applications, which still lag behind websites in searchability.

### Linkable

As another characteristic inherited from websites, a well-designed website should use the URI to indicate the current state of the application. This will enable the web app to retain or reload its state when the user bookmarks or shares the app’s URL.

### Responsive

A progressive web app’s UI must fit the device’s form factor and screen size.

### App-like

A progressive web app should look like a native app and be built on the application shell model, with minimal page refreshes.
Connectivity-independent. It should work in areas of low connectivity or offline (our favorite characteristic).

### Re-engageable.

Mobile app users are more likely to reuse their apps, and progressive web apps are intended to achieve the same goals through features such as push notifications.

### Installable.

A progressive web app can be installed on the device’s home screen, making it readily available.

### Fresh.

When new content is published and the user is connected to the 
Internet, that content should be made available in the app.

### Safe.

Because a progressive web app has a more intimate user experience and because all network requests can be intercepted through service workers, it is imperative that the app be hosted over HTTPS to prevent man-in-the-middle attacks.


## Nodejs and Golang based Web and API servers

Due to faster performance of Golang based API servers for fetching data from database server and serving to GraphQL client (React, Relay & GraphQL based application). Refer [here](https://github.com/sogko/golang-relay-starter-kit) for reference seed to use React+Relay with Golang(for GraphQL based API server) and Express.js(for application server).

## Cloud based orchestration of microservices.

Instead of building one monolithic stack of frontend and backend, we will use microservice based architecture wherein we do following:

* Define group of services for entire stack including groups like storage(Redis for session and cache, MongoDB for data storage and MySql for historical data), API (Golang based GraphQL), WEB (Nde.js + Express.js based application server)

* Using JWT for entire orchestration so that traffic across participating nodes of microservices are secured.

* Using encrypted cookie based browser session backed by Redis store for session management and controller concurrent access from multiple devices (desktop, mobile, tablet etc).

* Building and deploying Docker images for each microservice will be handled by pipeline services offered by vendors like GutHub, Butbucket or GitLab. So on every PR(pull request) merged to master will trigger the pipeline for automatic docker image creation from latest code of master branch and automatic deployment to Amazon ECS.

## Analytics: Measure, measure and measure.

* When we use analytics in web product we get to know lot of insights. These insights helps in changing the course of action of approach. For example, if load time is high in some region but userbase is large then it needs high attention to fix the load issue for that region. It can arise to be poor CDN configuration or due to network glitches in that particular area.

* If we know the problem, we can solve it. If we do not the problem then we are like blind person crossing the road in a hope that no issue will hit and everything will be just fine automatically. In real world as issue are inevitable and analytic provide strength to pro-active preparation for bad times.

* There are also some business and economic decision that can be data driven and most of the times are reasonably correct. If some feature/content is not well accepted in particular region, its better to change than hoping to expect that everyone's likes and dislike are same. Some are good with red, but many be more at ease with lighter shades of blue. All five fingers are not same and so our our end-users.

* Analytics is a never-ending task. Its like fuel that's needed till we are running business. Google Analytics provides something for all kind of usecases and requirements and they are leader in this domain. Better we use it from very beginning to have more accurate numbers while project starts milking return of investments.

# Showcase

A small part of the entire gammut is showcased just to provide and example which cover few concepts realted to building web applications.

## Introduction

This showcase is a small web application using front-end stack and cover folowing aspects:

* Usage of React.js for building UI components.

* Usage of react-router for (HTML5 history API based clean url) navigations 

* Use of babel for transpiling ES6 source.

* Webpack for bundling resources and emitting packaged bundle for production deployment.

* Use of geolocation and Google Map apis to location current city of end-user.

## How to run

```
    $ npm i
    $ npm start
```

## TODOs

* Adding Jest/enzyme based snapshot testing

* Adding istanbul for code coverage metrics.

* Setting up CI/CD pipeline on Gitlab. Refer [here](https://gitlab.com/help/topics/autodevops/index.md) for more details.

* ...



# References
https://developers.google.com/web/progressive-web-apps/

https://www.formula1.com/en.html

https://www.nascar.com/monster-energy-nascar-cup-series/2018/schedule/
